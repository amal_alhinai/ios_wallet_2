//
//  AFManager.swift
//  TelyPayIOS
//
//  Created by admin on 9/26/18.
//  Copyright © 2018 admin. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import CFNetwork

class AFManager : NSObject{
    
    
    
    class Connectivity {
        class var isConnectedToInternet:Bool {
            return NetworkReachabilityManager()!.isReachable
        }
    }
    

// // certificate pinning
//    class private func enableCertificatePinning()-> SessionManager{
//        var sessionManager: SessionManager?
//        let certificates = getCertificates()
//        let trustPolicy = ServerTrustPolicy.pinCertificates(
//            certificates: certificates,
//            validateCertificateChain: true,
//            validateHost: true)
//        let trustPolicies = [ "telypay.com": trustPolicy ]
//        let policyManager =  ServerTrustPolicyManager(policies: trustPolicies)
//        let configuration = URLSessionConfiguration.default
//        configuration.timeoutIntervalForRequest=20
//        sessionManager = SessionManager(
//            configuration: configuration,
//            serverTrustPolicyManager: policyManager)
//        return sessionManager!
//    }
//
//    class private func getCertificates() -> [SecCertificate] {
//        let url = Bundle.main.url(forResource: "telypayssl", withExtension: "cer")!
//        let localCertificate = try! Data(contentsOf: url) as CFData
//        guard let certificate = SecCertificateCreateWithData(nil, localCertificate)
//            else { return [] }
//
//        return [certificate]
//    }
    
    
    public static let sharedManager: SessionManager = {
        let configuration1 = URLSessionConfiguration.default
        configuration1.timeoutIntervalForRequest=40
        //let certificates = getCertificates()
        
        let key: SecKey? = getPubKey()
                let serverTrustPolicy = ServerTrustPolicy.pinPublicKeys(publicKeys: [key!],
                    validateCertificateChain: true,
                    validateHost: true)
        
                let serverTrustPolicies = [
                    "telypay.com": serverTrustPolicy
                ]
//        let trustPolicy = ServerTrustPolicy.pinCertificates(
//            certificates: certificates,
//            validateCertificateChain: true,
//            validateHost: true)
//        let trustPolicies = [ "telypay.com": trustPolicy ]
        let policyManager =  ServerTrustPolicyManager(policies: serverTrustPolicies)
        let manager = Alamofire.SessionManager(configuration: configuration1, serverTrustPolicyManager: policyManager)
        return manager
    }()

    
  // get public key from the certificate
    class private func getPubKey() -> SecKey? {
        let certificateData = NSData(contentsOf:Bundle.main.url(forResource: "telypayssl", withExtension: "cer")!)
        //print("iuiuiuiu \(String(describing: certificateData))")

        let certificate = SecCertificateCreateWithData(nil, certificateData!)

        var trust: SecTrust?

        let policy = SecPolicyCreateBasicX509()
        let status = SecTrustCreateWithCertificates(certificate!, policy, &trust)

        var key: SecKey?
        if status == errSecSuccess {
            key = SecTrustCopyPublicKey(trust!)!;
            print("NetworkImplementation :: getPubKey :: success")
        }
        return key
    }
    
    
    
    //request without header (get)
    //used this in check email if exist //also used in forgot password
    class func requestGETURL(_ strURL: String, success:@escaping (JSON) -> Void, failure:@escaping (Error) -> Void) {
        URLCache.shared.removeAllCachedResponses()
        
        // //certificate pinning
//        var sessionManager: SessionManager?
//        sessionManager =  enableCertificatePinning()
//
        
        // //publick key pinning
        //var sessionManager: SesstionManager?
//        let key: SecKey? = self.getPubKey()
//        let serverTrustPolicy = ServerTrustPolicy.pinPublicKeys(publicKeys: [key!],
//            validateCertificateChain: true,
//            validateHost: true)
//
//        let serverTrustPolicies = [
//            "telypay.com": serverTrustPolicy
//        ]
//
//        print("sssss \(serverTrustPolicies)")
//        sessionManager = SessionManager(configuration: URLSessionConfiguration.default,
//                                 serverTrustPolicyManager: ServerTrustPolicyManager(policies: serverTrustPolicies))
        
        sharedManager.request(strURL).responseJSON { (responseObject) -> Void in
            
           // print(responseObject)
            
            if responseObject.result.isSuccess {
                let resJson = JSON(responseObject.result.value!)
                success(resJson)
            }
            if responseObject.result.isFailure {
                let error : Error = responseObject.result.error!
                failure(error)
            }
        }
    }
    
    //with headers (get)
    //used this in get user details //used in change email// get registered devices
    class func requestGETURL(_ strURL : String, headers : [String : String]?, success:@escaping (JSON) -> Void, failure:@escaping (Error) -> Void){
        URLCache.shared.removeAllCachedResponses()
//        var sessionManager: SessionManager?
//        sessionManager =  enableCertificatePinning()
        sharedManager.request(strURL, method: .get, encoding: URLEncoding.httpBody , headers: headers).responseJSON { (responseObject) -> Void in
            
            //print(responseObject)
            
            if responseObject.result.isSuccess {
                let resJson = JSON(responseObject.result.value!)
                success(resJson)
            }
            if responseObject.result.isFailure {
                let error : Error = responseObject.result.error!
                let statusCode = responseObject.response?.statusCode
                //print(statusCode as Any)
                if statusCode == 200 {
                    AppDelegate.init().logoutWhenSessionExpire()
                }
                failure(error)
            }
        }
    }
    
    //////////////////////////////////////////////////////////////////
    //string request with headers (get)
    //used in getting the balance
    class func strRequestGETURL(_ strURL : String, headers : [String : String]?, success:@escaping (JSON) -> Void, failure:@escaping (Error) -> Void){
        URLCache.shared.removeAllCachedResponses()
//        var sessionManager: SessionManager?
//        sessionManager =  enableCertificatePinning()
        sharedManager.request(strURL, method: .get, encoding: URLEncoding.default , headers: headers).responseString { (responseObject) -> Void in
            
            //print(responseObject)
            
            if responseObject.result.isSuccess {
                let resJson = JSON(responseObject.result.value!)
                success(resJson)
            }
            
            if responseObject.result.isFailure {
                let error : Error = responseObject.result.error!
                let statusCode = responseObject.response?.statusCode
                //print(statusCode as Any)
                if statusCode == 200 {
                    AppDelegate.init().logoutWhenSessionExpire()
                }
                failure(error)
            }
            
        }
    }
    //////////////////////////////////////////////////////////////////////////////
    
    //json request with headers (post)
    //this used in top up
    class func requestPOSTURL(_ strURL : String, params : [String : AnyObject]?, headers : [String : String]?, success:@escaping (JSON) -> Void, failure:@escaping (Error) -> Void){
        URLCache.shared.removeAllCachedResponses()
//        var sessionManager: SessionManager?
//        sessionManager =  enableCertificatePinning()
        sharedManager.request(strURL, method: .post, parameters: params, encoding: JSONEncoding.default, headers: headers).responseJSON { (responseObject) -> Void in
            
            //print(responseObject)
            
            if responseObject.result.isSuccess {
                let resJson = JSON(responseObject.result.value!)
                success(resJson)
            }
            if responseObject.result.isFailure {
                let error : Error = responseObject.result.error!
                let statusCode = responseObject.response?.statusCode
                //print(statusCode as Any)
                if statusCode == 200 {
                    AppDelegate.init().logoutWhenSessionExpire()
                }
                failure(error)
            }
        }
    }
    
    
    ///without headers (post)
    //used this to registration
    class func requestPOSTURL(_ strURL : String, params : [String : AnyObject]?, success:@escaping (JSON) -> Void, failure:@escaping (Error) -> Void){
        URLCache.shared.removeAllCachedResponses()
//        var sessionManager: SessionManager?
//        sessionManager =  enableCertificatePinning()
        sharedManager.request(strURL, method: .post, parameters: params, encoding: URLEncoding.httpBody).responseJSON { (responseObject) -> Void in
            
            //print(responseObject)
            
            if responseObject.result.isSuccess {
                let resJson = JSON(responseObject.result.value!)
                success(resJson)
            }
            if responseObject.result.isFailure {
                let error : Error = responseObject.result.error!
                failure(error)
            }
        }
    }
    
    
    ///// response string (post)
    //used this in login // used in change password
    class func strRequestPOSTURL(_ strURL : String, params : [String : String]?, headers : [String : String]?, success:@escaping (JSON) -> Void, failure:@escaping (Error) -> Void){
        URLCache.shared.removeAllCachedResponses()
//        var sessionManager: SessionManager?
//        sessionManager =  enableCertificatePinning()
        sharedManager.request(strURL, method: .post, parameters: params, encoding: URLEncoding.httpBody, headers: headers).responseJSON { (response) in
            //print(response)
            
            if response.result.isSuccess {
                let resJson = JSON(response.result.value!)
                success(resJson)
            }
            if response.result.isFailure {
                let error : Error = response.result.error!
                let statusCode = response.response?.statusCode
                //print(statusCode as Any)
                if statusCode == 200 {
                    AppDelegate.init().logoutWhenSessionExpire()
                }
                failure(error)
            }
            
        }

      }
    
    
    //with headers (put)
    //used this in update id card img
    class func requestPUTURL(_ strURL : String, params : [String : String]?, headers : [String : String]?, success:@escaping (JSON) -> Void, failure:@escaping (Error) -> Void){
        URLCache.shared.removeAllCachedResponses()
//        var sessionManager: SessionManager?
//        sessionManager =  enableCertificatePinning()
        sharedManager.request(strURL, method: .put, parameters: params, encoding: JSONEncoding.default , headers: headers).responseJSON { (responseObject) -> Void in
            
            //print(responseObject)
            
            if responseObject.result.isSuccess {
                let resJson = JSON(responseObject.result.value!)
                success(resJson)
            }
            if responseObject.result.isFailure {
                let error : Error = responseObject.result.error!
                let statusCode = responseObject.response?.statusCode
                //print(statusCode as Any)
                if statusCode == 200 {
                    AppDelegate.init().logoutWhenSessionExpire()
                }
                failure(error)
            }
        }
    }
    
    //with headers (put)
    //no params
    //used this for testing// reset the accounts details
    class func requestTestPUTURL(_ strURL : String, headers : [String : String]?, success:@escaping (JSON) -> Void, failure:@escaping (Error) -> Void){
        URLCache.shared.removeAllCachedResponses()
//        var sessionManager: SessionManager?
//        sessionManager =  enableCertificatePinning()
        sharedManager.request(strURL, method: .put, encoding: JSONEncoding.default , headers: headers).responseJSON { (responseObject) -> Void in
            
            //print(responseObject)
            
            if responseObject.result.isSuccess {
                let resJson = JSON(responseObject.result.value!)
                success(resJson)
            }
            if responseObject.result.isFailure {
                let error : Error = responseObject.result.error!
                let statusCode = responseObject.response?.statusCode
                //print(statusCode as Any)
                if statusCode == 200 {
                    AppDelegate.init().logoutWhenSessionExpire()
                }
                failure(error)
            }
        }
    }
    
    //////////////////////////////////////////////////////////////////
    //with headers (get)
    //used this in get id card img and selfie img
    class func strRequestGETURL2(_ strURL : String, headers : [String : String]?, success:@escaping (JSON) -> Void, failure:@escaping (Error) -> Void){
        URLCache.shared.removeAllCachedResponses()
//        var sessionManager: SessionManager?
//        sessionManager =  enableCertificatePinning()
        sharedManager.request(strURL, method: .get, encoding: JSONEncoding.default , headers: headers).responseString { (responseObject) -> Void in
            
            //print(responseObject)
            
            if responseObject.result.isSuccess {
                let resJson = JSON(responseObject.result.value!)
                success(resJson)
            }
            if responseObject.result.isFailure {
                let error : Error = responseObject.result.error!
                let statusCode = responseObject.response?.statusCode
                //print(statusCode as Any)
                if statusCode == 200 {
                    AppDelegate.init().logoutWhenSessionExpire()
                }
                failure(error)
            }
        }
    }
    
    //with headers (post)// without params
    //used this in logout
    class func strRequestPOSTURL2(_ strURL : String, headers : [String : String]?, success:@escaping (JSON) -> Void, failure:@escaping (Error) -> Void){
        URLCache.shared.removeAllCachedResponses()
        //        var sessionManager: SessionManager?
        //        sessionManager =  enableCertificatePinning()
        sharedManager.request(strURL, method: .post, encoding: JSONEncoding.default , headers: headers).responseString { (responseObject) -> Void in
            
            //print(responseObject)
            
            if responseObject.result.isSuccess {
                let resJson = JSON(responseObject.result.value!)
                success(resJson)
            }
            if responseObject.result.isFailure {
                let error : Error = responseObject.result.error!
                let statusCode = responseObject.response?.statusCode
                //print(statusCode as Any)
                if statusCode == 200 {
                    AppDelegate.init().logoutWhenSessionExpire()
                }
                failure(error)
            }
        }
    }
////////////////////////////////////////////////////////////////////////////////
 
    
    
    //this method to change the messages that comes from server to strings that inside strings file for translation purpose
    class func changeMsg1(MsgCode : Int) -> String {
        var newMsgStr = ""
        if MsgCode != 0 {
            switch MsgCode {
            case 207:
                newMsgStr = NSLocalizedString("username_password_err_msg", comment: "")
                break;
            case 307:
                newMsgStr = NSLocalizedString("activation_msg", comment: "")
                break;
            case 305:
                newMsgStr = NSLocalizedString("only_end_users_use_the_app_msg", comment: "");
                break;
            case 202:
                newMsgStr = NSLocalizedString("successfully_topup_msg", comment: "")
                break;
            case 201:
                newMsgStr = NSLocalizedString("reach_max_balance_msg", comment: "")
                break;
            case 108:
                newMsgStr = NSLocalizedString("id_card_not_approved_msg", comment: "")
                break;
            case 107:
                newMsgStr = NSLocalizedString("id_card_not_valid_msg", comment: "")
                break;
            case 109:
                newMsgStr = NSLocalizedString("exceeded_num_of_allowed_top_up_req_msg", comment: "")
                break;
            case 105:
                newMsgStr = NSLocalizedString("wait_5_min_to_do_other_req_msg", comment: "")
                break;
            case 100:
                newMsgStr = NSLocalizedString("email_already_exist_msg", comment: "")
                break;
            case 209:
                newMsgStr = NSLocalizedString("email_has_been_sent_msg", comment: "")
                break;
            case 204:
                newMsgStr = NSLocalizedString("old_pass_same_to_new_pass_msg", comment: "")
                break;
            case 206:
                newMsgStr = NSLocalizedString("old_pass_incorrect_msg", comment: "")
                break;
            case 200:
                newMsgStr = NSLocalizedString("invalid_card_num_msg", comment: "")
                break;
            case 205:
                newMsgStr = NSLocalizedString("pass_has_been_change_msg", comment: "")
                break;
            case 403:
                newMsgStr =  NSLocalizedString("invalid_pass_msg", comment: "");
                break;
            case 404:
                newMsgStr = NSLocalizedString("invalid_civilId_msg", comment: "");
                break;
            case 405:
                newMsgStr = NSLocalizedString("invalid_phone_msg", comment: "");
                break;
            case 406:
                newMsgStr = NSLocalizedString("invalid_email_msg", comment: "");
                break;
            case 407:
                newMsgStr = NSLocalizedString("email_has_been_sent_msg", comment: "");
                break;
            case 102:
                newMsgStr = NSLocalizedString("success_register_dialog_msg", comment: "")
                break;
            case 101:
                newMsgStr = NSLocalizedString("failed_to_load_img", comment: "")
                break;
            case 304:
                newMsgStr = NSLocalizedString("img_updated_successfuly_msg", comment: "")
                break;
            case 301:
                newMsgStr = NSLocalizedString("removed_successfuly_msg", comment: "")
                break;
            case 309:
                newMsgStr = NSLocalizedString("lock_for_30_min_msg", comment: "")
                break;
            case 400:
                newMsgStr = NSLocalizedString("lock_for_1_hour_msg", comment: "")
                break;
            case 401:
                newMsgStr = NSLocalizedString("lock_for_3_hour_msg", comment: "")
                break;
            case 402:
                newMsgStr = NSLocalizedString("lock_for_6_hour_msg", comment: "")
                break;
            case 308:
                newMsgStr = NSLocalizedString("too_many_login_msg", comment: "")
                break;
            case 999:
                newMsgStr = NSLocalizedString("session_expired", comment: "")
                break;
            case 306:
                newMsgStr = NSLocalizedString("logged_in_from_other_device_msg", comment: "")
                break;
            case 408:
                //newMsgStr = mCtx.getString(R.string.reach_max_balance_msg);
                newMsgStr = NSLocalizedString("reach_max_balance_300_msg", comment: "");
                break;
            case 411:
                newMsgStr = NSLocalizedString("wait_2_for_topup_process", comment: "");
                break;
            case 410:
                newMsgStr = NSLocalizedString("wait_for_topup_process", comment: "");
                break;
//            case "An error has occurred.":
//                newMsgStr = NSLocalizedString("some_error_occured", comment: "")
//                break;
            default:
                newMsgStr = NSLocalizedString("some_error_occured", comment: "")
            }
            
        }else {
            newMsgStr = NSLocalizedString("some_error_occured", comment: "")
        }
        
        
        return newMsgStr
    }
    
//   
    
    
}
