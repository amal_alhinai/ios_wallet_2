//
//  DeviceTableViewCell.swift
//  TelyPayIOS
//
//  Created by admin on 11/8/18.
//  Copyright © 2018 admin. All rights reserved.
//

import UIKit

class DeviceTableViewCell: UITableViewCell {

    @IBOutlet weak var deviceTypeLbl: UILabel!
    @IBOutlet weak var deviceNameLbl: UILabel!
    @IBOutlet weak var removeBtn: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
