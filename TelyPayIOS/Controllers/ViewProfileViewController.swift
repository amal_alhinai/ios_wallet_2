//
//  ViewProfileViewController.swift
//  TelyPayIOS
//
//  Created by admin on 11/4/18.
//  Copyright © 2018 admin. All rights reserved.
//

import UIKit
import RSFloatInputView
import SkyFloatingLabelTextField

class ViewProfileViewController: BaseViewController {

    @IBOutlet weak var nameTxt: SkyFloatingLabelTextField!
    
    @IBOutlet weak var emailTxt: SkyFloatingLabelTextField!
    
    @IBOutlet weak var phoneNumTxt: SkyFloatingLabelTextField!
    
  
    @IBOutlet weak var pageTitle: UINavigationItem!
    
    //let keyChainWrapper = KeyChainWrapperManager()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        pageTitle.title = NSLocalizedString("view_profile_txt", comment: "")
        nameTxt.text = keyChainWrapper.getLoggedUserDetails().nameStr
        nameTxt.placeholder = "   " + NSLocalizedString("display_name_ed_hint", comment: "")
        nameTxt.isEnabled = false
        nameTxt.textColor = UIColor.white
        nameTxt.layer.borderWidth = 1
        nameTxt.layer.borderColor = UIColor.white.cgColor
        nameTxt.setLeftPaddingPoints(10.0)
        nameTxt.setRightPaddingPoints(10.0)
        
        emailTxt.text = keyChainWrapper.getLoggedUserDetails().emailStr
        emailTxt.placeholder = "   " + NSLocalizedString("email_ed_hint", comment: "")
        emailTxt.isEnabled = false
        emailTxt.textColor = UIColor.white
        emailTxt.layer.borderWidth = 1
        emailTxt.layer.borderColor = UIColor.white.cgColor
        emailTxt.setLeftPaddingPoints(10.0)
        emailTxt.setRightPaddingPoints(10.0)
        
        phoneNumTxt.text = keyChainWrapper.getLoggedUserDetails().phoneStr
        phoneNumTxt.placeholder = "   " + NSLocalizedString("phone_ed_hint", comment: "")
        phoneNumTxt.isEnabled = false
        phoneNumTxt.textColor = UIColor.white
        phoneNumTxt.layer.borderWidth = 1
        phoneNumTxt.layer.borderColor = UIColor.white.cgColor
        phoneNumTxt.setLeftPaddingPoints(10.0)
        phoneNumTxt.setRightPaddingPoints(10.0)
     
        
        // Do any additional setup after loading the view.
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
