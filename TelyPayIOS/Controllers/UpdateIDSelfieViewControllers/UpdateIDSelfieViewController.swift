//
//  UpdateIDSelfieViewController.swift
//  TelyPayIOS
//
//  Created by admin on 6/23/19.
//  Copyright © 2019 admin. All rights reserved.
//

import UIKit

class UpdateIDSelfieViewController: UIPageViewController, UIPageViewControllerDelegate  {

    
    var firstNameStr: String = ""
    var middleNameStr: String = ""
    var lastNameStr: String = ""
    var emailStr: String = ""
    var phoneNumStr: String = ""
    var civilIDNumStr: String = ""
    var passwordStr: String = ""
    var imgStr: String = ""
    var selfieImgStr: String = ""
    
    //pages of update idCard and selfie - page view controller
    var pages:[UIViewController] = [
        UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "updatePage1"),
        UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "updatePage2"),
        UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "updatePage3"),
        ]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        if let firstVC = pages.first
        {
            setViewControllers([firstVC], direction: .forward, animated: true, completion: nil)
           
        }
        
        
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
