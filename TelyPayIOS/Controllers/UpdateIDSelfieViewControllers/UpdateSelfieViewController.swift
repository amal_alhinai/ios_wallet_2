//
//  UpdateSelfieViewController.swift
//  TelyPayIOS
//
//  Created by admin on 6/23/19.
//  Copyright © 2019 admin. All rights reserved.
//

import UIKit
import AVFoundation


class UpdateSelfieViewController: BaseViewController, UIPageViewControllerDelegate, AVCapturePhotoCaptureDelegate {
    
    var captureSession: AVCaptureSession!
    var stillImageOutput: AVCapturePhotoOutput!
    var videoPreviewLayer: AVCaptureVideoPreviewLayer!
    var imgStr: String!
    var selfieImgStr: String!
    
    @IBOutlet weak var previewView: UIView!
    @IBOutlet weak var takeIDPhotoBtn: UIButton!
    @IBOutlet weak var capturedIDImg: UIImageView!
    @IBOutlet weak var btnsStackView: UIStackView!
    @IBOutlet weak var retakeBtn: UIButton!
    @IBOutlet weak var nextBtn: UIButton!
    @IBOutlet weak var backBtn: UIButton!
    
    //    @IBOutlet weak var switchCameraBtn: UIButton!
    //
    //
    //    @IBAction func switchCameraAction(_ sender: Any) {
    //
    //        if (captureSession != nil){
    //            let currentCameraInput: AVCaptureInput = captureSession.inputs[0]
    //            captureSession.removeInput(currentCameraInput)
    //            var newCamera: AVCaptureDevice
    //            newCamera = AVCaptureDevice.default(for: AVMediaType.video)!
    //
    //            if (currentCameraInput as! AVCaptureDeviceInput).device.position == .back {
    //                UIView.transition(with: self.previewView, duration: 0.5, options: .transitionFlipFromLeft, animations: {
    //                    newCamera = self.cameraWithPosition(.front)!
    //                }, completion: nil)
    //            } else {
    //                UIView.transition(with: self.previewView, duration: 0.5, options: .transitionFlipFromRight, animations: {
    //                    newCamera = self.cameraWithPosition(.back)!
    //                }, completion: nil)
    //            }
    //            do {
    //                try self.captureSession?.addInput(AVCaptureDeviceInput(device: newCamera))
    //            }
    //            catch {
    //                print("error: \(error.localizedDescription)")
    //            }
    //
    //        }
    //
    //    }
    
    
    
    func cameraWithPosition(_ position: AVCaptureDevice.Position) -> AVCaptureDevice? {
        let deviceDescoverySession = AVCaptureDevice.DiscoverySession.init(deviceTypes: [AVCaptureDevice.DeviceType.builtInWideAngleCamera], mediaType: AVMediaType.video, position: AVCaptureDevice.Position.unspecified)
        
        for device in deviceDescoverySession.devices {
            if device.position == position {
                return device
            }
        }
        return nil
    }
    

    @IBAction func goBackAction(_ sender: Any) {
        
        let parentVC = self.parent as! UpdateIDSelfieViewController
        
        // change page of PageViewController
        parentVC.setViewControllers([parentVC.pages[1]], direction: .reverse, animated: true, completion: nil)
        
    }
    
    @IBAction func nextBtnAction(_ sender: Any) {
//        let parentVC = self.parent as! UpdateIDSelfieViewController
        
        // change page of PageViewController
//        parentVC.setViewControllers([parentVC.pages[4]], direction: .reverse, animated: true, completion: nil)
//        SignUpViewController.pageControl.currentPage = 4
        
        saveImageRequest()
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        previewView.sendSubviewToBack(takeIDPhotoBtn)
        capturedIDImg.isHidden = true
        btnsStackView.isHidden = true
        //
        //        takeIDPhotoBtn.layer.borderColor =  _ColorLiteralType(red: 0.1006977562, green: 0.2920336034, blue: 0.5024970988, alpha: 1) as! CGColor
        takeIDPhotoBtn.layer.borderWidth = 1
        
        retakeBtn.setTitle(NSLocalizedString("rtake_pic_btn_txt", comment: ""), for: .normal)
        nextBtn.setTitle(NSLocalizedString("save", comment: ""), for: .normal)
        
        
        /// this code for change the arrow direction of backBtn img
        let locale = NSLocale.current.languageCode
        print(locale as Any)
        if locale == "ar" as String {
            backBtn.transform = backBtn.transform.rotated(by: .pi/1)
        }
        
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        
        captureSession = AVCaptureSession()
        captureSession.sessionPreset = .medium
        
        guard let frontCamera = AVCaptureDevice.default(AVCaptureDevice.DeviceType.builtInWideAngleCamera, for: .video, position: .front) else {
            return
        }
        
        do {
            let input = try AVCaptureDeviceInput(device: frontCamera)
            
            stillImageOutput = AVCapturePhotoOutput()
            
            if captureSession.canAddInput(input) && captureSession.canAddOutput(stillImageOutput) {
                captureSession.addInput(input)
                captureSession.addOutput(stillImageOutput)
                setupLivePreview()
            }
            
        }
        catch let error  {
            print("Error Unable to initialize front camera:  \(error.localizedDescription)")
        }
        
    }
    
    func setupLivePreview() {
        
        videoPreviewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        
        videoPreviewLayer.videoGravity = .resizeAspect
        videoPreviewLayer.connection?.videoOrientation = .portrait
        previewView.layer.addSublayer(videoPreviewLayer)
        
        DispatchQueue.global(qos: .userInitiated).async { //[weak self] in
            self.captureSession.startRunning()
            DispatchQueue.main.async {
                self.videoPreviewLayer.frame = self.previewView.bounds
            }
        }
        
    }
    
    @IBAction func takeIDPhotoAction(_ sender: Any) {
        
        previewView.isHidden = true
        capturedIDImg.isHidden = false
        btnsStackView.isHidden = false
        takeIDPhotoBtn.isHidden = true
        //        switchCameraBtn.isHidden = true
        btnsStackView.bringSubviewToFront(view)
        capturedIDImg.sendSubviewToBack(btnsStackView)
        
        if #available(iOS 11.0, *) {
            let settings = AVCapturePhotoSettings(format: [AVVideoCodecKey: AVVideoCodecType.jpeg])
            stillImageOutput.capturePhoto(with: settings, delegate: self)
            
        } else {
            // Fallback on earlier versions
            let settings = AVCapturePhotoSettings(format: [AVVideoCodecKey : AVVideoCodecJPEG])
            stillImageOutput.capturePhoto(with: settings, delegate: self)
        }
        
    }
    
    
    @available(iOS 11.0, *)
    func photoOutput(_ output: AVCapturePhotoOutput, didFinishProcessingPhoto photo: AVCapturePhoto, error: Error?) {
        
        guard let imageData = photo.fileDataRepresentation()
            else { return }
        
        let image = UIImage(data: imageData)
        capturedIDImg.image = image
        let selfieImgStr = TakeIDCardPhotoViewController.ConvertImageToBase64String(img: image!)
        //print("id card image string \(imgStr)")
        let parentVC = self.parent as! UpdateIDSelfieViewController
        parentVC.selfieImgStr = selfieImgStr
        
    }
    
    
    func photoOutput(_ output: AVCapturePhotoOutput, didFinishProcessingPhoto photoSampleBuffer: CMSampleBuffer?, previewPhoto previewPhotoSampleBuffer: CMSampleBuffer?, resolvedSettings: AVCaptureResolvedPhotoSettings, bracketSettings: AVCaptureBracketedStillImageSettings?, error: Error?) {
        
        if let sampleBuffer = photoSampleBuffer,
            let previewBuffer = previewPhotoSampleBuffer,
            let dataImage = AVCapturePhotoOutput.jpegPhotoDataRepresentation(forJPEGSampleBuffer: sampleBuffer, previewPhotoSampleBuffer: previewBuffer) {
            
            let image = UIImage(data: dataImage)
            capturedIDImg.image = image
            let selfieImgStr = TakeIDCardPhotoViewController.ConvertImageToBase64String(img: image!)
            //print("id card image string \(imgStr)")
            let parentVC = self.parent as! UpdateIDSelfieViewController
            parentVC.selfieImgStr = selfieImgStr
            
        }
        
    }
    
    
    @IBAction func retakeIDPhotoBtnAction(_ sender: Any) {
        previewView.isHidden = false
        capturedIDImg.isHidden = true
        btnsStackView.isHidden = true
        takeIDPhotoBtn.isHidden = false
        //        switchCameraBtn.isHidden = false
        
    }
    
    func saveImageRequest() {
        
        let parentVC = self.parent as! UpdateIDSelfieViewController
        imgStr = parentVC.imgStr
        
        let progressHUD = ProgressHUD(text: NSLocalizedString("please_wait", comment: ""))
        self.view.addSubview(progressHUD)
        progressHUD.show()
        
        let headers = ["Authorization":"bearer " + keyChainWrapper.getLoggedUserDetails().accessToken!,
                       "Content-Type":"application/json; charset=utf-8"]
        
        let params = ["IDCardPic": imgStr]
        
        if !AFManager.Connectivity.isConnectedToInternet {
            print("no internet is available.")
            AppDelegate.init().noInternetConnectionMsg(viewController: self)
        }
        
        AFManager.requestPUTURL(URLs.URL_UPDATE_ID_CARD, params: (params as! [String : String]), headers: headers, success: { (response) in
            progressHUD.hide()
            //print(response)
            
            self.updateSelfieRequest()
            
//            let alert = UIAlertController(title: "", message: AFManager.changeMsg1(MsgCode: response["resCode"].intValue), preferredStyle: UIAlertController.Style.alert)
//            alert.addAction(UIAlertAction(title: NSLocalizedString("ok", comment: ""), style: UIAlertAction.Style.default, handler: nil ))
//            self.present(alert, animated: true, completion: nil)
            
        }) { (error) in
            progressHUD.hide()
            // print(error)
            let alert = UIAlertController(title: "", message: NSLocalizedString("some_error_occured", comment: "") , preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("ok", comment: ""), style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    
    
    func updateSelfieRequest() {
        
        let parentVC = self.parent as! UpdateIDSelfieViewController
        selfieImgStr = parentVC.selfieImgStr
        
        let idCard = IDCard()
        idCard.imgStr = parentVC.imgStr
        idCard.stateStr = keyChainWrapper.getIdCard().stateStr
        idCard.selfieStr = parentVC.selfieImgStr
        self.keyChainWrapper.setIDCardUser(idCard)
        
        let progressHUD = ProgressHUD(text: NSLocalizedString("please_wait", comment: ""))
        self.view.addSubview(progressHUD)
        progressHUD.show()
        
        let headers = ["Authorization":"bearer " + keyChainWrapper.getLoggedUserDetails().accessToken!,
                       "Content-Type":"application/json; charset=utf-8"]
        
        let params = ["Selfie": selfieImgStr]
        
        if !AFManager.Connectivity.isConnectedToInternet {
            print("no internet is available.")
            AppDelegate.init().noInternetConnectionMsg(viewController: self)
        }
        
        AFManager.requestPUTURL(URLs.URL_UPDATE_SELFIE, params: (params as! [String : String]), headers: headers, success: { (response) in
            progressHUD.hide()
            //print(response)
            
            
                        let alert = UIAlertController(title: "", message: AFManager.changeMsg1(MsgCode: response["resCode"].intValue), preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("ok", comment: ""), style: UIAlertAction.Style.default, handler: { (action) -> Void in
                
                self.dismiss(animated: true, completion: nil)
                
            } ))
                        self.present(alert, animated: true, completion: nil)
            
        }) { (error) in
            progressHUD.hide()
            // print(error)
            let alert = UIAlertController(title: "", message: NSLocalizedString("some_error_occured", comment: "") , preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("ok", comment: ""), style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        
    }
    
    //this function to covert the image to string
    public static func ConvertImageToBase64String (img: UIImage) -> String {
        let imageData:NSData = img.jpegData(compressionQuality: 0.50)! as NSData //UIImagePNGRepresentation(img)
        let imgString = imageData.base64EncodedString(options: .init(rawValue: 0))
        return imgString
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.captureSession.stopRunning()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
