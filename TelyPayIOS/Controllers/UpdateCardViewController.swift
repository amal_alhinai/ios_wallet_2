//
//  UpdateCardViewController.swift
//  TelyPayIOS
//
//  Created by admin on 11/6/18.
//  Copyright © 2018 admin. All rights reserved.
//

import UIKit

class UpdateCardViewController: BaseViewController ,UIImagePickerControllerDelegate, UINavigationControllerDelegate{

    @IBOutlet weak var updatedCardImg: UIImageView!
    @IBOutlet weak var saveBtn: UIButton!
    @IBOutlet weak var updateCardBtn: UIButton!
    
    var imgStr: String!
    //let userDefault = UserDefaultManager()
    //let keyChainWrapper = KeyChainWrapperManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        saveBtn.dropShadow()
        updateCardBtn.dropShadow()
        updateCardBtn.setTitle(NSLocalizedString("change_id_card_txt", comment: ""), for: .normal)
        saveBtn.setTitle(NSLocalizedString("save", comment: ""), for: .normal)
        saveBtn.isEnabled = false
        updatedCardImg.image = toImage(str: keyChainWrapper.getIdCard().imgStr!)
        // Do any additional setup after loading the view.
    }
    
    @IBAction func updateCardBtnAction(_ sender: Any) {
        
        if (keyChainWrapper.getIdCard().stateStr == "Pending") || (keyChainWrapper.getIdCard().stateStr == "Rejected") {
         saveBtn.isEnabled = true
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = .camera;
            imagePicker.allowsEditing = false
            self.present(imagePicker, animated: true, completion: nil)
        }
      }
        else{
            saveBtn.isEnabled = false
            
            let alert = UIAlertController(title: "", message: NSLocalizedString("your_id_card_approved_msg", comment: ""), preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("ok", comment: ""), style: UIAlertAction.Style.default, handler: nil ))
            self.present(alert, animated: true, completion: nil)
            
        }
        
    }
    
    @IBAction func saveBtnAction(_ sender: Any) {
        
        saveImageRequest()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func saveImageRequest() {
        
        let progressHUD = ProgressHUD(text: NSLocalizedString("please_wait", comment: ""))
        self.view.addSubview(progressHUD)
        progressHUD.show()
        
        let headers = ["Authorization":"bearer " + keyChainWrapper.getLoggedUserDetails().accessToken!,
                       "Content-Type":"application/json; charset=utf-8"]
        
        let params = ["IDCardPic": imgStr]
       
        if !AFManager.Connectivity.isConnectedToInternet {
            print("no internet is available.")
            AppDelegate.init().noInternetConnectionMsg(viewController: self)
        }
        
        AFManager.requestPUTURL(URLs.URL_UPDATE_ID_CARD, params: (params as! [String : String]), headers: headers, success: { (response) in
            progressHUD.hide()
            //print(response)
            
            let alert = UIAlertController(title: "", message: AFManager.changeMsg1(MsgCode: response["resCode"].intValue), preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("ok", comment: ""), style: UIAlertAction.Style.default, handler: nil ))
             self.present(alert, animated: true, completion: nil)
            
        }) { (error) in
            progressHUD.hide()
           // print(error)
            let alert = UIAlertController(title: "", message: NSLocalizedString("some_error_occured", comment: "") , preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("ok", comment: ""), style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    
    func toImage(str : String) -> UIImage? {
        
        if let data1 = Data(base64Encoded: str, options: .ignoreUnknownCharacters){
            return UIImage(data: data1)
        }
        return nil
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
// Local variable inserted by Swift 4.2 migrator.
let info = convertFromUIImagePickerControllerInfoKeyDictionary(info)

        
        if let pickedImage = info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.originalImage)] as? UIImage {
                        updatedCardImg.contentMode = .scaleToFill
                        updatedCardImg.image = pickedImage
            imgStr = TakeIDCardPhotoViewController.ConvertImageToBase64String(img: pickedImage)
                    }
        
                    picker.dismiss(animated: true, completion: nil)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKeyDictionary(_ input: [UIImagePickerController.InfoKey: Any]) -> [String: Any] {
	return Dictionary(uniqueKeysWithValues: input.map {key, value in (key.rawValue, value)})
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKey(_ input: UIImagePickerController.InfoKey) -> String {
	return input.rawValue
}
