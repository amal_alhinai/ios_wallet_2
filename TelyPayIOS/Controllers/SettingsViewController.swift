//
//  SettingsViewController.swift
//  TelyPayIOS
//
//  Created by admin on 10/14/18.
//  Copyright © 2018 admin. All rights reserved.
//

import UIKit
import SwiftyJSON

class SettingsViewController: BaseViewController {

    
    @IBOutlet weak var viewProfileBtn: UIButton!
    @IBOutlet weak var changePassBtn: UIButton!
    @IBOutlet weak var changeEmailBtn: UIButton!
    @IBOutlet weak var updateCardBtn: UIButton!
    @IBOutlet weak var registeredDevicesBtn: UIButton!
    @IBOutlet weak var logoutBtn: UIButton!
    @IBOutlet weak var titleLbl: UIBarButtonItem!
    @IBOutlet weak var backBtn: UIBarButtonItem!
    @IBOutlet weak var topUpHistoryBtn: UIButton!
    var idImageStr = ""
    var selfieImageStr = ""
    var status = ""
    var historyListArr = [JSON]()

    
    
    //let userDefault = UserDefaultManager()
    //let keyChainWrapper = KeyChainWrapperManager()
    
    let idCard = IDCard()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        titleLbl.title = NSLocalizedString("settings", comment: "")
        viewProfileBtn.setTitle(NSLocalizedString("view_profile_txt", comment: ""), for: .normal)
        changePassBtn.setTitle(NSLocalizedString("change_password_txt", comment: ""), for: .normal)
        changeEmailBtn.setTitle(NSLocalizedString("change_email_txt", comment: ""), for: .normal)
        updateCardBtn.setTitle(NSLocalizedString("change_id_card_and_selfie_txt", comment: ""), for: .normal)
        registeredDevicesBtn.setTitle(NSLocalizedString("registered_devices_txt", comment: ""), for: .normal)
        logoutBtn.setTitle(NSLocalizedString("logout_txt", comment: ""), for: .normal)
        
        
        /// this code for change the arrow direction of backBtn img
        let locale = NSLocale.current.languageCode
        print(locale as Any)
        if locale == "ar" as String {
            backBtn.customView?.transform = (backBtn.customView?.transform.rotated(by: .pi/1))!
        }
        
        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func backBtnAction(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func updateCardBtnAction(_ sender: Any) {
        
        getCardImage()
        
    }
    
    
    @IBAction func logoutBtnAction(_ sender: Any) {
        
        AppDelegate.init().logout()
        
    }
    
   
    @IBAction func openHistoryBtnAction(_ sender: Any) {
        
        getTopUpHistory()
    }
    
    
    
    @IBAction func changeEmailBtnAction(_ sender: Any) {
        
        
        let alert = UIAlertController(title: NSLocalizedString("change_email_dilog_title", comment: ""), message: NSLocalizedString("change_email_dilog_msg", comment: ""), preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("yes", comment: ""), style: UIAlertAction.Style.default, handler: { (action: UIAlertAction!) in
            self.changeEmailRequest()
        }))
        alert.addAction(UIAlertAction(title: NSLocalizedString("no", comment: ""), style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
        
    }
    
    func changeEmailRequest() {
        
        let progressHUD = ProgressHUD(text: NSLocalizedString("please_wait", comment: ""))
        self.view.addSubview(progressHUD)
        progressHUD.show()
        
        let changeEmailURL = URLs.URL_CHANGE_EMAIL //+ keyChainWrapper.getLoggedUserDetails().emailStr!
        
        let headers = ["Authorization":"bearer " + keyChainWrapper.getLoggedUserDetails().accessToken!]
        
        if !AFManager.Connectivity.isConnectedToInternet {
            print("no internet is available.")
            AppDelegate.init().noInternetConnectionMsg(viewController: self)
        }
        
        AFManager.requestGETURL(changeEmailURL, headers: headers, success: { (response) in
            progressHUD.hide()
            //print(response["message"])
            
            let alert = UIAlertController(title: "", message: AFManager.changeMsg1(MsgCode: response["resCode"].intValue) , preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("ok", comment: ""), style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            
        })
        { (error) in
            progressHUD.hide()
            //print(error)
            let alert = UIAlertController(title: "", message: NSLocalizedString("some_error_occured", comment: "") , preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("ok", comment: ""), style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        
    }
    
    func getCardImage() {
        
        let progressHUD = ProgressHUD(text: NSLocalizedString("please_wait", comment: ""))
        self.view.addSubview(progressHUD)
        progressHUD.show()
        
        let headers = ["Authorization":"bearer " + keyChainWrapper.getLoggedUserDetails().accessToken!,
                       "Content-Type":"application/json; charset=utf-8"]
        
        if !AFManager.Connectivity.isConnectedToInternet {
            print("no internet is available.")
            AppDelegate.init().noInternetConnectionMsg(viewController: self)
        }
        
        AFManager.strRequestGETURL2(URLs.URL_GET_ID_CARD, headers: headers, success: { (response) in
            progressHUD.hide()
            //print(response)
            
            
            let json = JSON.init(parseJSON: response.stringValue)
            let image = json["IDimage"].stringValue
            let imgState = json["Status"].stringValue
            
            if image.isEmpty {
                
                AppDelegate.init().logoutWhenSessionExpire()
            }
            
            self.idImageStr = image
            self.status = imgState
            
            //            self.idCard.imgStr = image
            //            self.idCard.stateStr = imgState
            //            self.keyChainWrapper.setIDCardUser(self.idCard)
            
//            print("id card img \(String(describing: image))")
            //print("img status \(String(describing: imgState))")
            self.getSelfieImage()
            
            //            let updateCardPage = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "updateCardPage1") as? ViewIDAndSelfieViewController
            //            updateCardPage?.title = NSLocalizedString("change_id_card_txt", comment: "")
            //            self.navigationController?.pushViewController(updateCardPage!, animated: true)
            
        }) { (error) in
            progressHUD.hide()
            //print(error)
            let alert = UIAlertController(title: "", message: NSLocalizedString("some_error_occured", comment: "") , preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("ok", comment: ""), style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    
    func getSelfieImage() {
        
        let progressHUD = ProgressHUD(text: NSLocalizedString("please_wait", comment: ""))
        self.view.addSubview(progressHUD)
        progressHUD.show()
        
        let headers = ["Authorization":"bearer " + keyChainWrapper.getLoggedUserDetails().accessToken!,
                       "Content-Type":"application/json; charset=utf-8"]
        
        if !AFManager.Connectivity.isConnectedToInternet {
            print("no internet is available.")
            AppDelegate.init().noInternetConnectionMsg(viewController: self)
        }
        
        AFManager.strRequestGETURL2(URLs.URL_GET_SELFIE, headers: headers, success: { (response) in
            progressHUD.hide()
//            print("hkhkhkhkkjkjk \(response)")
            
            
            let json = JSON.init(parseJSON: response.stringValue)
            let image = json["Selfieimage"].stringValue
            
            if image.isEmpty {

                AppDelegate.init().logoutWhenSessionExpire()
            }
            
            self.selfieImageStr = image
            
            self.idCard.imgStr = self.idImageStr
            self.idCard.stateStr = self.status
            self.idCard.selfieStr = self.selfieImageStr
            self.keyChainWrapper.setIDCardUser(self.idCard)
            
//            print("selfie img \(String(describing: image))")
            //print("img status \(String(describing: imgState))")
            
            
            let updateCardPage = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "updateCardPage") as? ViewIDAndSelfieViewController
            updateCardPage?.title = NSLocalizedString("change_id_card_and_selfie_txt", comment: "")
            self.navigationController?.pushViewController(updateCardPage!, animated: true)
            
        }) { (error) in
            progressHUD.hide()
            //print(error)
            let alert = UIAlertController(title: "", message: NSLocalizedString("some_error_occured", comment: "") , preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("ok", comment: ""), style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    
    
    func getTopUpHistory() {
                
                let progressHUD = ProgressHUD(text: NSLocalizedString("please_wait", comment: ""))
                self.view.addSubview(progressHUD)
                progressHUD.show()
                
                let headers = ["Content-Type":"application/json; charset=utf-8",
                               "Authorization":"bearer " + keyChainWrapper.getLoggedUserDetails().accessToken!]
                
                if !AFManager.Connectivity.isConnectedToInternet {
                    print("no internet is available.")
                    AppDelegate.init().noInternetConnectionMsg(viewController: self)
                }
                
                AFManager.requestGETURL(URLs.URL_GET_TOPUP_HISTORY, headers: headers, success: { (JSONresponse) in
                    
                    progressHUD.hide()
                    print("rrrssss \(JSONresponse)")

                    self.historyListArr = JSONresponse.arrayValue
//                    print("rtrt \(self.historyListArr[2])")
//                    print("sssizeeee \(self.historyListArr.count)")
                    
                    let HistoryPage = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "historyPage") as? HistoryTableViewController
                    HistoryPage?.title = NSLocalizedString("show_history", comment: "")
                    HistoryPage?.historyListArr = self.historyListArr
                    self.navigationController?.pushViewController(HistoryPage!, animated: true)

                    
                }) { (error) in
                    
                    progressHUD.hide()
        //            print(error)
                    let alert = UIAlertController(title: "", message: NSLocalizedString("some_error_occured", comment: "") , preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction(title: NSLocalizedString("ok", comment: ""), style: UIAlertAction.Style.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
                
            
        
        
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
