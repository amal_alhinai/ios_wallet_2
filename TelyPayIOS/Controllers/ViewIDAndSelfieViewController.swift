//
//  ViewIDAndSelfieViewController.swift
//  TelyPayIOS
//
//  Created by admin on 6/17/19.
//  Copyright © 2019 admin. All rights reserved.
//

import UIKit

class ViewIDAndSelfieViewController: BaseViewController , UINavigationControllerDelegate{
    
    @IBOutlet weak var idCardImg: UIImageView!
    @IBOutlet weak var selfieImg: UIImageView!
    @IBOutlet weak var idCardBtn: UIButton!
    @IBOutlet weak var selfieBtn: UIButton!
    
    @IBOutlet weak var updateIDSelfieBtn: UIButton!
    @IBOutlet weak var imageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        updateIDSelfieBtn.setTitle(NSLocalizedString("change_id_card_and_selfie_txt", comment: ""), for: .normal)
        imageView.image = toImage(str: keyChainWrapper.getIdCard().imgStr!)
        idCardImg.image = toImage(str: keyChainWrapper.getIdCard().imgStr!)
        selfieImg.image = toImage(str: keyChainWrapper.getIdCard().selfieStr!)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        imageView.image = toImage(str: keyChainWrapper.getIdCard().imgStr!)
        idCardImg.image = toImage(str: keyChainWrapper.getIdCard().imgStr!)
        selfieImg.image = toImage(str: keyChainWrapper.getIdCard().selfieStr!)
        
        
    }
    
    @IBAction func showIDImg(_ sender: Any) {
        
        imageView.image = toImage(str: keyChainWrapper.getIdCard().imgStr!)
    }
    
    @IBAction func showSelfieImg(_ sender: Any) {
        
        imageView.image = toImage(str: keyChainWrapper.getIdCard().selfieStr!)
    }
    
    @IBAction func updateIdAndSelfieAction(_ sender: Any) {
        
        
        if (keyChainWrapper.getIdCard().stateStr == "Pending") || (keyChainWrapper.getIdCard().stateStr == "Rejected") {

            //
            let updatePage = self.storyboard?.instantiateViewController(withIdentifier: "updatePages") as? UpdateIDSelfieViewController
            self.present(updatePage!, animated: true, completion: nil)
            
        }
        else{

            let alert = UIAlertController(title: "", message: NSLocalizedString("your_id_card_approved_msg", comment: ""), preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("ok", comment: ""), style: UIAlertAction.Style.default, handler: nil ))
            self.present(alert, animated: true, completion: nil)

        }
        
        
    }
    
    
    func toImage(str : String) -> UIImage? {
        
        if let data1 = Data(base64Encoded: str, options: .ignoreUnknownCharacters){
            return UIImage(data: data1)
        }
        return nil
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
