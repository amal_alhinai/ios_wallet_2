//
//  ProfileViewController.swift
//  TelyPayIOS
//
//  Created by admin on 9/25/18.
//  Copyright © 2018 admin. All rights reserved.
//

import UIKit
import CFNetwork

class ProfileViewController: BaseViewController {
    
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var balanceLbl: UILabel!
    @IBOutlet weak var toolbarView: UIView!
    @IBOutlet weak var topUpBtn: UIButton!
    @IBOutlet weak var instructionLbl: UITextView!
    @IBOutlet weak var currentBalanceLbl: UILabel!
    @IBOutlet weak var omrLbl: UILabel!
    
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    var currentBalance : Float = 0.0
    let refreshControl = UIRefreshControl()
    //let userDefaul = UserDefaultManager()
    //let keyChainWrapper = KeyChainWrapperManager()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        refreshControl.addTarget(self, action: #selector(didPullToRefresh), for: .valueChanged)
        refreshControl.tintColor = UIColor.white
        scrollView.addSubview(refreshControl)
        
        
        // Do any additional setup after loading the view.
        topUpBtn.dropShadow()
        topUpBtn.setTitle(NSLocalizedString("top_up", comment: ""), for: .normal)
        currentBalanceLbl.text = NSLocalizedString("current_balance_tv", comment: "")
        omrLbl.text = NSLocalizedString("omr_tv", comment: "")
        //let userDefault = UserDefaultManager()
        currentBalance = keyChainWrapper.getLoggedUserDetails().balance!
        //print("acccesssss toookeeeen  \(String(describing: keyChainWrapper.isUserLoggedIN()))")
        
        nameLbl.text = keyChainWrapper.getLoggedUserDetails().nameStr
        balanceLbl.text = NSString(format: "%.3f", keyChainWrapper.getLoggedUserDetails().balance ?? 0.000) as String
        
        checkBalance()
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        getUpdatedBalance()
        checkBalance()
    }
    
    @objc func didPullToRefresh() {
        print("refresh done")
        refreshControl.beginRefreshing()
        getUpdatedBalance()
        checkBalance()
        
    }
    
    func checkBalance() {
        if currentBalance <= 1.0 {
            lowBalanceColorsAndText()
        } else {
            highBalanceColorsAndText()
        }
    }
    
    func highBalanceColorsAndText() {
    //
        view.backgroundColor = UIColor(red: 30/255.0, green: 88/255, blue: 130/255, alpha: 1.0)
        toolbarView.backgroundColor = UIColor(red: 26/255.0, green: 74/255, blue: 128/255, alpha: 1.0)
         topUpBtn.setTitleColor (UIColor(red: 30/255.0, green: 88/255, blue: 130/255, alpha: 1.0), for: UIControl.State.normal)
        //instructionLbl.text = NSLocalizedString("you_can_purchase_online_using_telypay_balance_tv", comment: "")

        let attributedString = NSMutableAttributedString(string: NSLocalizedString("you_can_purchase_online_using_telypay_balance_tv", comment: ""))
        let linkRange = (attributedString.string as NSString).range(of: NSLocalizedString("partnered_stores_tv", comment: ""))
        attributedString.addAttribute(.link, value: URLs.BASE_URL, range: linkRange)
        attributedString.addAttribute(.font, value: UIFont.boldSystemFont(ofSize: 17), range: linkRange)
        
        let linkAttributes: [String : Any] = [
            NSAttributedString.Key.foregroundColor.rawValue: UIColor(red: 254/255.0, green: 209/255, blue: 44/255, alpha: 1.0),
            NSAttributedString.Key.underlineColor.rawValue: UIColor.clear,
            NSAttributedString.Key.underlineStyle.rawValue: NSUnderlineStyle.single.rawValue
        ]
        
        instructionLbl.linkTextAttributes = convertToOptionalNSAttributedStringKeyDictionary(linkAttributes)
        instructionLbl.attributedText = attributedString
        instructionLbl.font =  UIFont.systemFont(ofSize: 17)
        instructionLbl.textAlignment = .center
        instructionLbl.textColor = UIColor.white
        
    }
    
    func lowBalanceColorsAndText() {
      //
        view.backgroundColor = UIColor(red: 221/255.0, green: 64/255, blue: 85/255, alpha: 1.0)
        toolbarView.backgroundColor = UIColor(red: 98/255.0, green: 62/255, blue: 82/255, alpha: 1.0)
        topUpBtn.setTitleColor(UIColor(red: 221/255.0, green: 64/255, blue: 85/255, alpha: 1.0), for: UIControl.State.normal)
        instructionLbl.text = NSLocalizedString("low_balance_tv", comment: "")
    }
    
    
    func getUpdatedBalance() {
        
//        let progressHUD = ProgressHUD(text: "Please wait ...")
//        self.view.addSubview(progressHUD)
        //progressHUD.show()
        
        //let getBalanceUrlStr = URLs.URL_GET_BALANCE + userDefaul.getLoggedUserDetails().emailStr!
        //print(getBalanceUrlStr)
        let headers  = ["Authorization":"bearer " + keyChainWrapper.getLoggedUserDetails().accessToken!,
                        "Content-Type":"application/json"]
        //print(headers)
        
        if !AFManager.Connectivity.isConnectedToInternet {
            print("no internet is available.")
            AppDelegate.init().noInternetConnectionMsg(viewController: self)
        }
        
        AFManager.strRequestGETURL(URLs.URL_GET_BALANCE, headers: headers, success: { (response) in
            //progressHUD.hide()
            
            //print("responseee  \(response.floatValue)")
            
            if response.stringValue.count > 8 {
                
                AppDelegate.init().logoutWhenSessionExpire()
            }
           
                self.keyChainWrapper.updateBlance(balance: response.floatValue)
                self.balanceLbl.text = NSString(format: "%.3f", self.keyChainWrapper.getLoggedUserDetails().balance ?? 0.000) as String
                self.balanceLbl.reloadInputViews()
                self.currentBalance = self.keyChainWrapper.getLoggedUserDetails().balance!
                self.checkBalance()
            
            self.refreshControl.endRefreshing()
            
        }) { (error) in
            //progressHUD.hide()
            //print(error)
            self.refreshControl.endRefreshing()
            let alert = UIAlertController(title: "", message: NSLocalizedString("some_error_occured", comment: "") , preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("ok", comment: ""), style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            
        }
        
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToOptionalNSAttributedStringKeyDictionary(_ input: [String: Any]?) -> [NSAttributedString.Key: Any]? {
	guard let input = input else { return nil }
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (NSAttributedString.Key(rawValue: key), value)})
}
