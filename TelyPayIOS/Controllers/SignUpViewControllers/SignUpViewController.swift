//
//  SignUpViewController.swift
//  TelyPayIOS
//
//  Created by admin on 10/1/18.
//  Copyright © 2018 admin. All rights reserved.
//

import UIKit

class SignUpViewController: UIPageViewController, UIPageViewControllerDelegate {
    
    var isIPhoneX: Bool = false
    
    var firstNameStr: String = ""
    var middleNameStr: String = ""
    var lastNameStr: String = ""
    var emailStr: String = ""
    var phoneNumStr: String = ""
    var civilIDNumStr: String = ""
    var passwordStr: String = ""
    var imgStr: String = ""
    var selfieImgStr: String = ""
    
    
    //pages of sign up - page view controller
    var pages:[UIViewController] = [
        UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "signupPage1"),
        UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "signupPage2"),
        UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "signupPage3"),
        UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "signupPage4"),
        UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "signupPage5"),
    ]
    
    ///constraints for title label in other devices
    lazy var titleConstraints: [NSLayoutConstraint] = [
        NSLayoutConstraint(item: SignUpViewController.titleLbl, attribute: .left, relatedBy: .equal, toItem: self.view, attribute: .left, multiplier: 1, constant: 0),
        NSLayoutConstraint(item: SignUpViewController.titleLbl, attribute: .right, relatedBy: .equal, toItem: self.view, attribute: .right, multiplier: 1, constant: 0),
        NSLayoutConstraint(item: SignUpViewController.titleLbl, attribute: .top, relatedBy: .equal, toItem: self.view, attribute: .top, multiplier: 1, constant: 0),
        NSLayoutConstraint(item: SignUpViewController.titleLbl, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 90)
    ]
    
    ///constraints for title label in iphonex
    lazy var titleConstraintsForX: [NSLayoutConstraint] = [
        NSLayoutConstraint(item: SignUpViewController.titleLbl, attribute: .left, relatedBy: .equal, toItem: self.view, attribute: .left, multiplier: 1, constant: 0),
        NSLayoutConstraint(item: SignUpViewController.titleLbl, attribute: .right, relatedBy: .equal, toItem: self.view, attribute: .right, multiplier: 1, constant: 0),
        NSLayoutConstraint(item: SignUpViewController.titleLbl, attribute: .top, relatedBy: .equal, toItem: self.view, attribute: .top, multiplier: 1, constant: 0),
        NSLayoutConstraint(item: SignUpViewController.titleLbl, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 135)
    ]
 
    
    
    public static var pageControl = CustomImagePageControl()
    public static var titleLbl = UILabel()

    override func viewDidLoad() {
        super.viewDidLoad()

        self.delegate   = self
        
        
        ///make the first page is create account page
        if let firstVC = pages.first
        {
            setViewControllers([firstVC], direction: .forward, animated: true, completion: nil)
            configureTitleLbl(pageTitle:NSLocalizedString("header_sign_up_create_account", comment: ""))
            
            isIPhoneX = AppDelegate.init().isIPhoneXDevice()
            
            self.configurePageControl()
            
            /// here adding the constraints based on the device
            if isIPhoneX
            {
                self.view.addConstraints(titleConstraintsForX)
                
            }
            else
            {
                self.view.addConstraints(titleConstraints)
               
            }
        }
        
        
        // Do any additional setup after loading the view.
    }
    
    override func viewDidLayoutSubviews() {
        
        self.updateTitle()
    }
    
    
    
    func configurePageControl() {
        if !isIPhoneX {
        SignUpViewController.pageControl = CustomImagePageControl(frame: CGRect(x: 0, y: UIScreen.main.bounds.minY + 100, width: UIScreen.main.bounds.width, height: 30))
        }else{
            SignUpViewController.pageControl = CustomImagePageControl(frame: CGRect(x: 0, y: UIScreen.main.bounds.minY + 150, width: UIScreen.main.bounds.width, height: 30))
        }
  
        SignUpViewController.pageControl.numberOfPages = pages.count
        SignUpViewController.pageControl.currentPage = 0
        SignUpViewController.pageControl.alpha = 1
        SignUpViewController.pageControl.transform = CGAffineTransform(scaleX: 2.5, y: 2.5)

        self.view.addSubview(SignUpViewController.pageControl)
        
    }
    
    

    func configureTitleLbl(pageTitle: String) {
        SignUpViewController.titleLbl = UILabel(frame: CGRect(x: 0, y: UIScreen.main.bounds.maxY - 633, width: UIScreen.main.bounds.width , height: 30))
        SignUpViewController.titleLbl.text = pageTitle
        SignUpViewController.titleLbl.translatesAutoresizingMaskIntoConstraints =  false
        SignUpViewController.titleLbl.textAlignment = .center
        SignUpViewController.titleLbl.textColor = #colorLiteral(red: 0.9997864366, green: 0.8452324271, blue: 0.2200273275, alpha: 1)
        self.view.addSubview(SignUpViewController.titleLbl)
    }
    
    func updateTitle() {
        
        if SignUpViewController.pageControl.currentPage == 0 {
            SignUpViewController.titleLbl.text = NSLocalizedString("header_sign_up_create_account", comment: "")
        }else if SignUpViewController.pageControl.currentPage == 1 {
            SignUpViewController.titleLbl.text = NSLocalizedString("header_sign_up_kyc_instruction", comment: "")
        }else if SignUpViewController.pageControl.currentPage == 2 {
            SignUpViewController.titleLbl.text = NSLocalizedString("header_sign_up_take_card_photo", comment: "")
        }else if SignUpViewController.pageControl.currentPage == 3 {
            SignUpViewController.titleLbl.text = NSLocalizedString("header_sign_up_take_selfie", comment: "")
        }else if SignUpViewController.pageControl.currentPage == 4 {
            SignUpViewController.titleLbl.text = NSLocalizedString("header_sign_up_terms_conditions", comment: "")
        }
        
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

