//
//  ViewController.swift
//  TelyPayIOS
//
//  Created by admin on 9/25/18.
//  Copyright © 2018 admin. All rights reserved.
//

import UIKit
import RSFloatInputView

class ViewController: BaseViewController, UITextFieldDelegate {

    @IBOutlet weak var usernameTxt: RSFloatInputView!
    @IBOutlet weak var passwordTxt: RSFloatInputView!
    
    @IBOutlet weak var loginBtn: UIButton!
    @IBOutlet weak var createAccountBtn: UIButton!
    @IBOutlet weak var emailErrorLbl: UILabel!
    @IBOutlet weak var passwordErrorLbl: UILabel!
    @IBOutlet weak var forgotPassBtn: UIButton!
    @IBOutlet weak var dontHaveAccountLbl: UILabel!
    
    let userDefaul = UserDefaultManager()
    //let keyChainWrapper = KeyChainWrapperManager()
    let currentUser = User()
    var mobileID = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        usernameTxt.textField.delegate = self
        passwordTxt.textField.delegate = self
        usernameTxt.textField.returnKeyType = .done
        passwordTxt.textField.returnKeyType = .done
        
        forgotPassBtn.setTitle(NSLocalizedString("forgot_password", comment: ""), for: .normal)
        dontHaveAccountLbl.text = NSLocalizedString("new_to_telypay_text", comment: "")
        
        usernameTxt.dropShadow()
        usernameTxt.layer.borderColor = UIColor(red: 26/255.0, green: 74/255, blue: 128/255, alpha: 1.0).cgColor
        passwordTxt.layer.borderColor = UIColor(red: 26/255.0, green: 74/255, blue: 128/255, alpha: 1.0).cgColor
        usernameTxt.placeHolderLabel.string = NSLocalizedString("email_ed_hint", comment: "")
        passwordTxt.dropShadow()
        passwordTxt.placeHolderLabel.string = NSLocalizedString("password_ed_hint", comment: "")
        loginBtn.dropShadow()
        loginBtn.setTitle(NSLocalizedString("login_btn_text", comment: ""), for: .normal)
        createAccountBtn.dropShadow()
        createAccountBtn.setTitle(NSLocalizedString("sign_up_btn_text", comment: ""), for: .normal)
        
        usernameTxt.textField.keyboardType = .emailAddress
        passwordTxt.textField.isSecureTextEntry = true

        usernameTxt.textField.addTarget(self, action: #selector(ViewController.textFieldDidChange(_:)),
                            for: UIControl.Event.editingChanged)
        
        passwordTxt.textField.addTarget(self, action: #selector(ViewController.textFieldDidChange(_:)),
                                        for: UIControl.Event.editingChanged)
        
        mobileID = (UIDevice.current.identifierForVendor?.uuidString)!
        
    }
    
    
    @objc func textFieldDidChange(_ textField: UITextField) {
    
        if textField == usernameTxt.textField {
            emailErrorLbl.isHidden = true
        }
        
        if textField == passwordTxt.textField {
            passwordErrorLbl.isHidden = true
        }
        
        
        
    }
    

    @IBAction func loginBtnAction(_ sender: Any) {
        
        if (usernameTxt.textField.text?.isEmpty)! || (passwordTxt.textField.text?.isEmpty)!   {
            
            if (usernameTxt.textField.text?.isEmpty)!{
                emailErrorLbl.isHidden = false
                emailErrorLbl.text = NSLocalizedString("error_string", comment: "")
            }
            
            if (passwordTxt.textField.text?.isEmpty)!{
                passwordErrorLbl.isHidden = false
                passwordErrorLbl.text = NSLocalizedString("error_string", comment: "")
            }
            
            
        }else {
        
        onLogin()
            
        }
    }

    
    func onLogin() {
        
        let progressHUD = ProgressHUD(text: NSLocalizedString("please_wait", comment: ""))
        self.view.addSubview(progressHUD)
        progressHUD.show()
       
        //print(mobileID)
//        print(" deviceeee toooken \(String(describing: UserDefaults.standard.string(forKey: "kDeviceToken")!))")
        let deviceToken = String(describing: UserDefaults.standard.string(forKey: "kDeviceToken")!)
        
        let parameters = ["username": usernameTxt.textField.text!,
                          "password": passwordTxt.textField.text! ,
                          "grant_type": "password" ,
                          "MobileID": mobileID,
                          "DeviceToken": deviceToken,
                          "Platform": "IOS"]
        let headers = ["Content-Type":"application/x-www-form-urlencoded"]
      
        //print(parameters)
        if !AFManager.Connectivity.isConnectedToInternet {
            print("no internet is available.")
            AppDelegate.init().noInternetConnectionMsg(viewController: self)
        }
        
        AFManager.strRequestPOSTURL(URLs.URL_LOGIN, params: parameters as [String : String],headers: headers ,success: { (JSONResponse) in
            progressHUD.hide()
            
            if(JSONResponse["error_description"] == .null){
            
                let accessToken = JSONResponse["access_token"]
                let accessTokenExpIn = JSONResponse["expires_in"]
                
                self.userDefaul.alreadyLoggedInBefore(x: 1)
                
                self.currentUser.accessToken = accessToken.stringValue
                self.currentUser.accessTokenExpireIn = accessTokenExpIn.intValue
                self.currentUser.nameStr = ""
                self.currentUser.phoneStr = ""
                self.currentUser.balance = 0.0
                self.currentUser.emailStr = ""
                self.currentUser.activationStatus = 0
                
                
                //print("user \(String(describing: self.currentUser.accessToken))")
                
                
                self.keyChainWrapper.setCurrentLoginUser(self.currentUser)
                
                self.getUserDetails(username: self.usernameTxt.textField.text!)
           
                
            }
            else {
                //print("err disc \(JSONResponse["error_description"])")
                
                //// this for taking the "block account error msg" and the "number of minits of blocking the
                if (JSONResponse["error_description"].stringValue.contains(":")){
                    
                    let msg = JSONResponse["error_description"].stringValue.split(separator: ":")
                    //print(msg[0])
                    let min = msg[1].replacingOccurrences(of: "-", with: "")
                    //print(min)
                    
                    let alert = UIAlertController(title: "", message: AFManager.changeMsg1(MsgCode: JSONResponse["error"].intValue) + " " + min + NSLocalizedString("min", comment: ""), preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction(title: NSLocalizedString("ok", comment: ""), style: UIAlertAction.Style.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                    
                }else
                {
                    let alert = UIAlertController(title: "", message: AFManager.changeMsg1(MsgCode: JSONResponse["error"].intValue), preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: NSLocalizedString("ok", comment: ""), style: UIAlertAction.Style.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                }
            
          }
            
            
        }) { (error) in
            progressHUD.hide()
            //print("error : \(error)")
            
            let alert = UIAlertController(title: "", message: NSLocalizedString("some_error_occured", comment: "") , preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("ok", comment: ""), style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            
        }
        
    }
    
    
    
    
    ////this method for get user details from server
    func getUserDetails(username : String){
        
        let getUserDetailsURLStr = URLs.URL_GET_USER_DETAILS //+ username
        let headers = ["Authorization":"bearer " + self.keyChainWrapper.getLoggedUserDetails().accessToken!]
        
        //print("ghghghgh" + self.keyChainWrapper.getLoggedUserDetails().accessToken!)
        if !AFManager.Connectivity.isConnectedToInternet {
            print("no internet is available.")
            AppDelegate.init().noInternetConnectionMsg(viewController: self)
        }
        AFManager.requestGETURL(getUserDetailsURLStr, headers: headers, success: { (response) in
            //print("user details response \(response)")
            
            let jsonArr = response[0]
            
            let name = jsonArr["Name"]
            let email = jsonArr["email"]
            let balance = jsonArr["balance"]
            let phone = jsonArr["phone"]
            let accountStatus = jsonArr["accountsatat"]
            
            self.currentUser.accessToken = self.keyChainWrapper.getLoggedUserDetails().accessToken
            self.currentUser.accessTokenExpireIn = self.keyChainWrapper.getLoggedUserDetails().accessTokenExpireIn
            self.currentUser.nameStr = name.stringValue
            self.currentUser.phoneStr = phone.stringValue
            self.currentUser.balance = balance.floatValue
            self.currentUser.emailStr = email.stringValue
            self.currentUser.activationStatus = accountStatus.intValue
            
            
            self.keyChainWrapper.setCurrentLoginUser(self.currentUser)
            //print("user \(String(describing: self.currentUser))")
            //print("user name \(String(describing: self.currentUser.nameStr))")
            
            
            let profilePage = self.storyboard?.instantiateViewController(withIdentifier: "profilePage") as? ProfileViewController
            self.present(profilePage!, animated: true, completion: nil)
            
            
        }) { (error) in
            
            //print(error)
            let alert = UIAlertController(title: "", message: NSLocalizedString("some_error_occured", comment: "") , preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("ok", comment: ""), style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
        
    }
    
    //will be called when the user touches any where in the view
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        //hide the keyboard from username field
        
        guard let touch:UITouch = touches.first else
        {
            return;
        }
        if touch.view != touch
        {
            usernameTxt.endEditing(true)
            passwordTxt.endEditing(true)   
            
        }
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}


