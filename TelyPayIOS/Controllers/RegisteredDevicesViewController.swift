//
//  RegisteredDevicesViewController.swift
//  TelyPayIOS
//
//  Created by admin on 11/8/18.
//  Copyright © 2018 admin. All rights reserved.
//

import UIKit
import SwiftyJSON

class RegisteredDevicesViewController: BaseViewController{
    
    @IBOutlet weak var registeredOneDeviceLbl: UILabel!
    @IBOutlet weak var removeMobBtn: UIButton!
    @IBOutlet weak var removeWebBtn: UIButton!
    @IBOutlet weak var mobileLbl: UILabel!
    @IBOutlet weak var webLbl: UILabel!
    @IBOutlet weak var pageTitle: UINavigationItem!
    
    //let userDefault = UserDefaultManager()
    //let keyChainWrapper = KeyChainWrapperManager()
    
    var devices = [String: Any]()
    
    var strMobileIP : String?
    var strComputerIP : String?
    
    @IBOutlet weak var mobileDeviceView: UIView!
    @IBOutlet weak var webDeviceView: UIView!
    
    @IBOutlet weak var mobileIDLbl: UILabel!
    
    @IBOutlet weak var webIDLbl: UILabel!

    @IBAction func removeDeviceBtnAction(_ sender: UIButton) {
        
        
        let alert = UIAlertController(title: NSLocalizedString("remove_device_dailog_title", comment: ""), message: NSLocalizedString("remove_device_dailog_msg", comment: ""), preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("yes", comment: ""), style: UIAlertAction.Style.default, handler: { (action: UIAlertAction!) in
            
        if sender.tag == 111 {
            
            self.removeDeviceRequest(mobileIP: self.strMobileIP!, computerIP: "")
            
        }
        else {
            self.removeDeviceRequest(mobileIP: "", computerIP: self.strComputerIP!)
        }
            
                    }))
                    alert.addAction(UIAlertAction(title: NSLocalizedString("no", comment: ""), style: UIAlertAction.Style.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
        
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        pageTitle.title = NSLocalizedString("registered_devices_txt", comment: "")
        mobileDeviceView.isHidden = true
        webDeviceView.isHidden = true
        self.getRegisteredDevices()
        registeredOneDeviceLbl.text = NSLocalizedString("you_can_only_be_logged_into_1_device_tv", comment: "")
        mobileLbl.text = NSLocalizedString("mobile_tv", comment: "")
        webLbl.text = NSLocalizedString("web_tv", comment: "")
        removeMobBtn.setTitle(NSLocalizedString("remove_btn_txt", comment: ""), for: .normal)
        removeWebBtn.setTitle(NSLocalizedString("remove_btn_txt", comment: ""), for: .normal)
        

        // Do any additional setup after loading the view.
    }
    
//    override func viewWillAppear(_ animated: Bool) {
//
//        getRegisteredDevices()
//
//    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func removeDeviceRequest(mobileIP: String, computerIP: String) {
 
            let progressHUD = ProgressHUD(text: NSLocalizedString("please_wait", comment: ""))
            self.view.addSubview(progressHUD)
            progressHUD.show()
            
            let params = ["MobileIP": mobileIP,
                          "ComputerIP": computerIP]
            //print(params)
            
            let headers = ["Authorization":"bearer " + self.keyChainWrapper.getLoggedUserDetails().accessToken!,
                           "Content-Type":"application/json; charset=utf-8"]
        
        if !AFManager.Connectivity.isConnectedToInternet {
            print("no internet is available.")
            AppDelegate.init().noInternetConnectionMsg(viewController: self)
        }
        
            AFManager.requestPUTURL(URLs.URL_REMOVE_DEVICE, params: params, headers: headers, success: { (response) in
                progressHUD.hide()
                //print(response)
                
                if params["MobileIP"] != "" {
                    
                    let alert = UIAlertController(title: NSLocalizedString("remove_device_dailog_title", comment: ""), message: AFManager.changeMsg1(MsgCode: response["resCode"].intValue), preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction(title: NSLocalizedString("ok", comment: ""), style: UIAlertAction.Style.default, handler: { (action: UIAlertAction!) in
                        
//                        self.keyChainWrapper.logout()
                        AppDelegate.init().logout()
//                        let loginPage = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as? ViewController
//                        self.present(loginPage!, animated: true, completion: nil)
                        
                    }))
                    self.present(alert, animated: true, completion: nil)
                    
                } else {
                    
                    let alert = UIAlertController(title: NSLocalizedString("remove_device_dailog_title", comment: ""), message: AFManager.changeMsg1(MsgCode: response["resCode"].intValue), preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction(title: NSLocalizedString("ok", comment: ""), style: UIAlertAction.Style.default, handler: { (action: UIAlertAction!) in
                        
                        self.webDeviceView.isHidden = true
                        
                    }))
                    self.present(alert, animated: true, completion: nil)
                    
                }
                
            }) { (error) in
                progressHUD.hide()
                //print(error)
                let alert = UIAlertController(title: "", message: NSLocalizedString("some_error_occured", comment: "") , preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: NSLocalizedString("ok", comment: ""), style: UIAlertAction.Style.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
  
        
    }
    
    
    func getRegisteredDevices() {
        
        let progressHUD = ProgressHUD(text: NSLocalizedString("please_wait", comment: ""))
        self.view.addSubview(progressHUD)
        progressHUD.show()
        
        let headers = ["Authorization":"bearer " + keyChainWrapper.getLoggedUserDetails().accessToken!,
                       "Content-Type":"application/json; charset=utf-8"]
        //print(headers)
        let getDevicesUrl = URLs.URL_GET_DEVICE //+ keyChainWrapper.getLoggedUserDetails().emailStr!
        
        if !AFManager.Connectivity.isConnectedToInternet {
            print("no internet is available.")
            AppDelegate.init().noInternetConnectionMsg(viewController: self)
        }
        
        AFManager.requestGETURL(getDevicesUrl, headers: headers, success: { (response) in
            
            progressHUD.hide()
            //print(response)
            let jsonObjResponse = response[0]
            //print(jsonObjResponse)
            self.devices = jsonObjResponse.dictionaryObject! as [String:Any]
            //print(self.devices["MobileIP"]!)
            
            self.strMobileIP = self.devices["MobileIP"]! as? String
            self.strComputerIP = self.devices["ComputerIP"]! as? String
            
            if (self.strMobileIP == nil) {
                self.mobileDeviceView.isHidden = true
                
            }else{
                self.mobileDeviceView.isHidden = false
                self.mobileIDLbl.text = self.devices["MobileIP"] as? String
            }
            
            if (self.strComputerIP == nil) {
                self.webDeviceView.isHidden = true
            }else{
                self.webDeviceView.isHidden = false
                self.webIDLbl.text = self.devices["ComputerIP"] as? String
            }
            
            
        }) { (error) in
            
            progressHUD.hide()
           // print(error)
            let alert = UIAlertController(title: "", message: NSLocalizedString("some_error_occured", comment: "") , preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("ok", comment: ""), style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            
        }
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
