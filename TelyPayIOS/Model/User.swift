//
//  User.swift
//  TelyPayIOS
//
//  Created by admin on 10/8/18.
//  Copyright © 2018 admin. All rights reserved.
//

import UIKit

class User: NSObject {

    
    var emailStr: String?
    var nameStr: String?
    var phoneStr: String?
    var passwordStr: String?
    var accessToken: String?
    var accessTokenExpireIn: Int?
    var balance: Float?
    var activationStatus: Int?
    
    override init() {
        ///
    }
    
    init(emailStr: String, nameStr: String, phoneStr: String, accessToken: String, accessTokenExpireIn: Int, balance: Float, activationStatus: Int) {
        
        
        self.emailStr = emailStr
        self.nameStr = nameStr
        self.phoneStr = phoneStr
        self.accessToken = accessToken
        self.accessTokenExpireIn = accessTokenExpireIn
        self.balance = balance
        self.activationStatus = activationStatus
        
        
    }
  

    
}
